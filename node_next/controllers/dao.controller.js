const DB = (function() {

   function Employee(employeeObj) {
            this.email = employeeObj.email;
            this.password = employeeObj.password;
            this.userType = employeeObj.userType;


    }
    var EmployeeTable = [];

    var getAllEmployees = function() {
        return EmployeeTable;
    }

    var getEmployee = function(username) {

       let employee =  EmployeeTable.find((element,index) => {
            return element.email === username;
        });

        return employee;
        
    }

    var addEmployee = function(employee) {
        console.log(employee);
        // let employeeId = 1;
        // console.log(EmployeeTable);
        // console.log(EmployeeTable.length);
        // if(EmployeeTable.length > 0) {
        //      employeeId = EmployeeTable[EmployeeTable.length -1].id + 1;
        // }

        // console.log(employeeId);
        
        // employee.id = employeeId;
        EmployeeTable.push( new Employee(employee))  ;
        return employee.email;
    }

    var updateEmployee = function(employee) {

        let index = EmployeeTable.findIndex(element => {
            return element.id = parseInt(employee.id);
        });
        
        EmployeeTable[index].name = employee.name;
        EmployeeTable[index].phone = employee.phone;

        return EmployeeTable[index];
      }

      var deleteEmployee = function(Id) {
          let index = EmployeeTable.findIndex(element => {
              return element.id === parseInt(Id);
          });
          console.log(index);
          EmployeeTable.splice(index,1);
          return Id;
      }

      var verifyEmployee = function(email,password) {
        let employee =  EmployeeTable.find((element,index) => {
            return element.email === email && element.password === password;
        });

        if(employee) {
            return employee;
        } else {
            return undefined;
        }
      }

    return {
        'addEmployee' : addEmployee,
        'getAllEmployees' : getAllEmployees,
        'getEmployee': getEmployee,
        'deleteEmployee' : deleteEmployee,
        'updateEmployee' : updateEmployee,
        'verifyEmployee' : verifyEmployee,
    };
})();

module.exports = DB;