let jwt = require('jsonwebtoken');
var DB = require('../controllers/dao.controller');
let config = require('../configs/jwt.config');

let authenticateUser = (req, res, next) => {
    let username = req.body.email;
    let password = req.body.password;

    if (username && password) {
        let employee =  DB.verifyEmployee(username,password);
        if (employee) {
          let token = jwt.sign({username: username},
            config.secret,
            { expiresIn: '24h' // expires in 24 hours
            }
          );
          // return the JWT token for the future API calls
          res.json({
            status: true,
            token: token
          });
        } else {
          res.sendStatus(403);
        }
      } else {
        res.send(400).json({
          status: false,
        });
      }
};

module.exports = {
    authenticateUser: authenticateUser
};