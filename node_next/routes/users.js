var express = require('express');
var DB = require('../controllers/dao.controller');
var router = express.Router();
let jwt = require('jsonwebtoken');
let config = require('../configs/jwt.config');
let middleware = require('../middleware/middleware.js');
let authHandler = require('../auth/auth.handler');
let validator = require('../helpers/validator');

/* GET users listing. */
router.get('/', function(req, res, next) {
    res.send('respond with a resource');
});

router.post('/signup', function(req, res, next) {
    let validEmail = validator.validateEmail(req.body.email);
    if (validEmail === true) {
        let employeeId = DB.addEmployee(req.body);
        res.json({
            status: true
        });
    } else {
        res.json({
            status: false,
            message: 'please enter a valid emailId'
        });
    }

});

router.post('/login', authHandler.authenticateUser);


router.get('/emp', middleware.checkToken, function(req, res, next) {
    if (req.decoded.username) {
        let employee = DB.getEmployee(req.decoded.username);
        if (employee) {
            if (employee.userType === 'employee') {
                res.send('Welcome to employee page');
            } else {
                res.sendStatus(403);
            }
        } else {
            res.sendStatus(403);
        }
    }
});

router.get('/admin', middleware.checkToken, function(req, res, next) {
    if (req.decoded.username) {
        let employee = DB.getEmployee(req.decoded.username);
        if (employee) {
            if (employee.userType === 'admin') {
                res.send('Welcome to admin page');
            } else {
                res.sendStatus(403);
            }
        } else {
            res.sendStatus(403);
        }
    }
});

router.get('/all', function(req, res, next) {
    let employees = DB.getAllEmployees();
    res.json(employees);
});

router.get('/:Id', function(req, res, next) {
    console.log(req.params);
    let employee = DB.getEmployee(req.params.Id);
    res.json(employee);
});

router.post('/update', function(req, res, next) {

    let employee = DB.updateEmployee(req.body);

    res.send(`employee with Id ${employee.id} updated succesfully`);
});

router.get('/remove/:Id', function(req, res, next) {
    console.log(req.params);
    let employeeId = DB.deleteEmployee(req.params.Id);

    res.send(`employee with Id ${employeeId} removed succesfully `);
});

module.exports = router;
